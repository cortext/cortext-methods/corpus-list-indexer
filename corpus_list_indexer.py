# -*- coding: utf-8 -*-
#!/usr/bin/python

from cortextlib.legacy import containerize

import os, sys
reload(sys) 
sys.setdefaultencoding("utf-8")
from sqlite3 import *
import types
sys.path.append("lib")
import shutil
import re
from librarypy.path import *
from librarypy import fonctions
import math
import copy
import codecs
import unicodedata
import csv
import pickle
import re as r
from operator import itemgetter
import sqlite3
import random
import nltk
from copy import deepcopy
import logging,pprint

#############################################################################
#############################################################################
#################################-FONCTIONS-#################################
#############################################################################
#############################################################################

def safe_execute(cur, query):
    try:
        cur.execute(query)
        print('Executed query: %s' % query)
    except:
        print('Not Executed query: %s' % query)
        pass

def create_custom_class(dbname,table_name='custom_class'):
	conn = sqlite3.connect(dbname)
	cur = conn.cursor()

	# create journals table
	safe_execute(cur, "DROP TABLE "+table_name)
	safe_execute(cur, "CREATE TABLE "+table_name+" (file TEXT)")
	#safe_execute(cur, "ALTER TABLE custom_class ADD COLUMN file TEXT")
	safe_execute(cur, "ALTER TABLE "+table_name+" ADD COLUMN id INTEGER")
	safe_execute(cur, "ALTER TABLE "+table_name+" ADD COLUMN rank INTEGER")
	safe_execute(cur, "ALTER TABLE "+table_name+" ADD COLUMN parserank INTEGER")
	safe_execute(cur, "ALTER TABLE "+table_name+" ADD COLUMN data TEXT")
	
	conn.commit()
	return conn,cur

def create_articles2terms(dbname,table_name='custom_class'):
	table_name=(table_name + 'articles2terms').replace("custom_class","")
	conn = sqlite3.connect(dbname)
	cur = conn.cursor()

	# create journals table
	safe_execute(cur, "DROP TABLE "+table_name)
	safe_execute(cur, "CREATE TABLE "+table_name+" (id INTEGER PRIMARY KEY)")
	safe_execute(cur, "ALTER TABLE "+table_name+" ADD COLUMN file TEXT")
	safe_execute(cur, "ALTER TABLE "+table_name+" ADD COLUMN wos_id TEXT")
	safe_execute(cur, "ALTER TABLE "+table_name+" ADD COLUMN terms_id INTEGER")
	safe_execute(cur, "ALTER TABLE "+table_name+" ADD COLUMN title_or_abstract INTEGER")
	safe_execute(cur, "ALTER TABLE "+table_name+" ADD COLUMN sentence_id INTEGER")
	conn.commit()
	return conn,cur

def create_terms(dbname,table_name='custom_class'):
	conn = sqlite3.connect(dbname)
	cur = conn.cursor()
	table_name=(table_name + 'terms').replace("custom_class","")
	
	print 'create table terms: ' + dbname
	safe_execute(cur, "DROP TABLE "+table_name)
	safe_execute(cur, "CREATE TABLE "+table_name+" (id INTEGER PRIMARY KEY)")
	safe_execute(cur, "ALTER TABLE "+table_name+" ADD COLUMN stemmed_key  TEXT")
	safe_execute(cur, "ALTER TABLE "+table_name+" ADD COLUMN term TEXT")
	safe_execute(cur, "ALTER TABLE  "+table_name+" ADD COLUMN variations TEXT")
	conn.commit()
	return conn,cur


###0.data extraction








def load_data(file_name = "toxico.csv",table_name='ISITITLE',file_type="csv",cursor='all',random=False,limit=False,years=False):
	#N = count_rows_where(name_bdd,'billets'," where jours IN ('" + "','".join(list(map(str,years))) + "') ")	
	corpus={}
	print table_name
	if file_type=='csv':
		#reader = csv.reader(open("toxico.csv", "rb"))
		reader = UnicodeReader(open("toxico.csv", "rb"))
		for row in reader:
			corpus[int(row[0])]= row[1]
	elif file_type=='sqlite':
		#print 'ici'
		conn = sqlite3.connect(file_name)
		cur = conn.cursor()
		rand_str,limit_str,where=' ',' ',' '
		if not random ==False or not limit==False:
			rand_str=' order by RANDOM() '
		if not limit==False:
			limit_str=' limit ' + str(limit) + ' '
		if not years==False:
			where = " where jours IN ('" + "','".join(list(map(str,years))) + "') "
		if cursor=='all':
			#print cur.execute('SELECT count(id) from ISITITLE ' + where),' entries with at least a title'
			query = 'SELECT id,data from ' +table_name + ' ' + rand_str + limit_str + where
			print query
			data=cur.execute(query)#.fetchall()
			i=0
			for dat in data:
				i+=1
				if not i%10000:
					print i, ' records read'
				corpus.setdefault(dat[0],[]).append(dat[1].replace('’',"'"))#on évite les guillemets pourris
			return corpus
#			#uniquement sur la dernière requête soit l'abstract...
		else:
			query = 'SELECT id,data from  '+ table_name + rand_str + ' ' + limit_str 
			print query
			cur.execute(query)
			return corpus,conn,cur
	elif file_type=='ex':
		corpus[0]= 'The phylogenetic position of the elephant shark (Callorhinchus milii) is particularly relevant to study the evolution of genes and gene regulation in vertebrates.'
		return corpus
	print '+++ corpus data loaded'		
	
	
def flat(list):
	list_flat=[]
	for l in list:
		list_flat += l
	return list_flat
	


#7.indexing
def check_present(sentence,chaine):
	for x in chaine:
		if x in sentence:
			return True
	return False

def check_present_w(sentence_w,reg_w):
	for x in reg_w:
		if x.issubset(sentence_w):
			return True
	return False

class dialect(csv.excel):
	delimiter = "\t"
	#delimiter = ";"
	skipinitialspace = False
	quotechar = '"'
	doublequote = False
	lineterminator='\n'
	quoting = False


#############################################################################
#############################################################################
##########################-LOADING PARAMETERS-###############################
#############################################################################
#############################################################################

try:
	print user_parameters
except:
	user_parameters=''
parameters_user=fonctions.load_parameters(user_parameters)
corpus_file = parameters_user.get('corpus_file','')
result_path=parameters_user.get('result_path','')
#terms_list=parameters_user.get('terms_list',os.path.join(result_path,'lexical_analysis/multiterms_statistics.csv'))
null_tag=parameters_user.get("null_tag",False)
file_eq_name=parameters_user.get('file_eq_name','')
merge_it=parameters_user.get('merge_it',True)
language=parameters_user.get('language',"en")
if language=='other':
	weird=True
else:
	weird=False
print 'path:'
print corpus_file
print result_path
result_path0=result_path[:]
fonctions.progress(result_path0,0)
	
#################################
###logging user parameters#######
#################################
logging.basicConfig(filename=os.path.join(result_path,'.user.log'), filemode='w', level=logging.DEBUG,format='%(asctime)s %(levelname)s : %(message)s', datefmt="%Y-%m-%d %H:%M:%S")
logging.info('Script Corpus List Indexer Started')
yamlfile = 'corpus_list_indexer.yaml'
if 'cointet' in os.getcwd():
	yamlfile = '/Users/jpcointet/Desktop/cortext-methods/corpus_terms_indexer/'+yamlfile
parameterslog=fonctions.getuserparameters(yamlfile,parameters_user)
logging.info(parameterslog)
fonctions.progress(result_path0,2)
##################################
### end logging user parameters###
##################################

corpus_type=parameters_user.get('corpus_type','isi')
try:
	print 'here we are'
	from librarypy import descriptor
	data=descriptor.get_descriptor(corpus_file)
	print 'data',data
	corpus_type=data['corpus_type']
except:
	corpus_type='other'

fields=[parameters_user.get('fields_2_index','')]
print 'fields_2_index',fields

dico_champs = {}
#dico_champs['comm_texte'] = 'comm_texte'

dico_champs = {}
if corpus_type=='isi':
	dico_champs['Keywords'] = 'ISIkeyword'
	dico_champs['Cited Author'] = 'ISICRAuthor'
	dico_champs['Cited Journal'] = 'ISICRJourn'
	dico_champs['Cited References'] = 'ISICitedRef'
	dico_champs['References'] = 'ISIRef'
	dico_champs['Research Institutions'] = 'ISIC1Inst'
	dico_champs['Subject Category'] = 'ISISC'
	dico_champs['Countries'] = 'ISIC1Country'
	dico_champs['Journal'] = 'ISIJOURNAL'
	dico_champs['Journal (long)'] = 'ISISO'
	dico_champs['Cities'] = 'ISIC1City'
	dico_champs['Author'] = 'ISIAUTHOR'
	dico_champs['Terms'] = 'ISIterms'
	dico_champs['Cited References'] = 'ISICitedRef'
	dico_champs['cp'] = 'cp'
	dico_champs['Country'] = 'Country'
	dico_champs['Year'] = 'ISIpubdate'
	dico_champs['Periods'] = 'ISIpubdate_custom'
	dico_champs['Keywords'] = 'ISIkeyword'
	dico_champs['Publication Type'] = 'ISIDT'
	dico_champs['Periods'] = 'ISIpubdate_custom'
	dico_champs['Funding'] = 'ISIFU'


dico_champs['Terms']='ISIterms'
dico_champs['Periods'] = 'ISIpubdate_custom'
if corpus_type=='xmlpubmed' or corpus_type=='isi' or corpus_type=='ris' :
	dico_champs['Year'] = 'ISIpubdate'
else:
	dico_champs['Time Steps'] = 'ISIpubdate'
dico_champs['texte'] = 'texte'
dico_champs_inv = {}
for x,y in dico_champs.iteritems():
	dico_champs_inv[y]=x
	
indexed_table_name = parameters_user.get('indexed_table_name','').replace('-','_').replace('%','_').replace('^','_').replace("'",'_').replace('&','_').replace('(','_').replace('{','_').replace('}','_').replace('+','_').replace('-','_').replace('/','_').replace(' ','_').replace(']','_').replace('[','_').replace('"','_').replace('.','_')
print 'indexed_table_name',indexed_table_name
print 'corpus_type',corpus_type
fields_bis=[]
fields= list(map(lambda x: dico_champs.get(x,x) , fields))
print 'fields',fields
method={}
for key,val in parameters_user.iteritems():
	method[key]=val
print 'parameters:', method
bdd_name=corpus_file
db_name=bdd_name
field=fields[0]
result_path_list=os.path.join(result_path,'list')
try:
	os.mkdir(os.path.join(result_path,'list'))
except:
	pass




#index_list_with_equivalence_dictionnary(field,list_file,equivalence_list_file)
def read_liste(list_file,read_mode='list',tolerance='moderate'):
	print "trying to read", list_file
	if read_mode=='list':
		data=[]
	else:
		data={}
	feed=codecs.open(list_file,'r','utf-8')
	firstline=feed.readline()
	feed.close()
	feed=codecs.open(list_file,'r','utf-8')
	if '\t' in firstline or tolerance=='moderate':
		#rowreader = csv.reader(open(list_file),dialect=dialect)
		rowreader = csv.reader(feed,dialect=dialect)
	else:
		try:
			rowreader = map(lambda x: x.split(';'),feed.read().split('\r'))
		except:
			logging.debug('File is not correctly formatted, have you checked encoing of your tsv file is indeed utf8 ?')
			jdsql
		#rowreader = map(lambda x: x.split(';'),open(list_file).read().split('\r'))
	i=0
	for row in rowreader:
		i+=1
		if i>1:
			cle=unicode(row[0])
			if read_mode=='list':
				data.append(cle)
			else:
				#print 'row',row
				try:
					for r in row[1].split('***'):
						#print r
						if len(r)>0:
							data.setdefault(cle,[]).append(unicode(r.strip()))
				except:
					if tolerance=='strict':
						logging.debug('your equivalence file is not correctly formatted, it should be tabulation separated, this row has an issue for instance: ' + pprint.pformat(row))
						jdsql
					else:
						try:
							data.setdefault(cle,[]).append(True)
						except:
							pass
				#data.setdefault(row[1],[]).append(row[0])
	print 'len(data)',len(data)
	return data


#index_list_with_equivalence_dictionnary(field,list_file,equivalence_list_file)
def read_liste2(list_file,read_mode='dict',nd=True):#read_liste2 use the first column as a key to build a dict with the next columns as values (inside the same list)
	print "trying to read", list_file
	data={}
	feed=codecs.open(list_file,'r','utf-8')
	firstline=feed.readline()
	feed.close()
	feed=codecs.open(list_file,'r','utf-8')
	
	tolerance="unknown"
	print 'tolerance',tolerance
	if '\t' in firstline or tolerance=='moderate':
		print 'inside first loop'
		#rowreader = csv.reader(open(list_file),dialect=dialect)
		rowreader = csv.reader(feed,dialect=dialect)
	else:
		print 'oustide first loop'
		try:
			if nd:
				rowreader = map(lambda x: x.split(';'),feed.read().split('\r'))
			else:
				print 'trying the classic way'
				rowreader = csv.reader(feed)
		except:
			try:
				rowreader = csv.reader(feed)
			except:
				logging.debug('File is not correctly formatted, have you checked encoing of your tsv file is indeed utf8 ?')
				jdsql
		#rowreader = map(lambda x: x.split(';'),open(list_file).read().split('\r'))
	i=0
	for row in rowreader:
		i+=1
		if i==1:
			legende = row[1:]
		else:
			cle=unicode(row[0])
			#print 'cledsjlkq',cle
			try:
				for col in row[1:]:
					temp_list_col=[]
					for r in col.split('***'):
						if len(r.strip())>0:
							temp_list_col.append(unicode(r.strip()))
					data.setdefault(cle,[]).append(temp_list_col)
			except:
				logging.debug('your equivalence file is not correctly formatted, it should be tabulation separated, this row has an issue for instance: ' + pprint.pformat(row))
			#data.setdefault(row[1],[]).append(row[0])
	print 'len(data)',len(data)
	return data,legende




if 'sjean-philippecointet/Desktop/' in os.getcwd():
	list_file=parameters_user.get('list_file',os.path.join(result_path_list,'list_'+field+'.tsv'))
	equivalence_file=parameters_user.get('equivalence_file',os.path.join(result_path_list,'suspected_equivalence_'+field+'.tsv'))
else:
	list_file_activated=parameters_user.get('list_file_activated','')
	if list_file_activated:
		list_file=parameters_user.get('list_file','')
	else:
		list_file=''
	
	#list_file=parameters_user.get('list_file','')
	equivalence_file_activated=parameters_user.get('equivalence_file_activated','')
	if equivalence_file_activated:
		equivalence_file=parameters_user.get('equivalence_file','')
	else:
		equivalence_file=''

if equivalence_file==None:
	equivalence_file=''
if list_file==None:
	list_file=''


print 'list_file',list_file
if not list_file=='':
	logging.info('Reading list file: '+list_file)
	fonctions.progress(result_path0,5)
	
	try:
		dictionnaire_field=read_liste(list_file,read_mode='dict')
	except:
		logging.debug('Please check your tsv file again, is it UTF8 encoded? ' + list_file)
		logger = logging.getLogger()
		logger.handlers[0].flush()
		dksql
		
	logging.info('using term list as provided in the file '+ list_file)
	logging.info('including items (first 10 elements):'+ pprint.pformat(dictionnaire_field.keys()[:10]))
		
	#print 'dictionnaire_field',dictionnaire_field
else:
	logging.info('Retrieving possible entities from '+field)
	fonctions.progress(result_path0,5)
	
	conn,curs=fonctions.create_bdd(bdd_name)
	try:
		corpus = fonctions.get_results(field,curs)
	except:
		logging.debug('Error, no field to index was selected')
		logger = logging.getLogger()
		logger.handlers[0].flush()
		dksql
	print 'extracting every data from ', field, ' in db : ',bdd_name
	dictionnaire_field = {}
	for id,nodes_list in  corpus.iteritems():
		for node in nodes_list:
			dictionnaire_field[unicode(node)]=dictionnaire_field.get(unicode(node),0)+1
			#dictionnaire_field[node]=dictionnaire_field.get(node,0)+1

#	print 'ori',x
liste=dictionnaire_field.keys()
#for u in  liste:
# 	if 'sénateur' in u:
# 		print 'udjklsqsjq',u
print 'liste size',len(liste)
#print liste
print 'alors equivalence_file',equivalence_file
#c'est la qu'il faut gérer les synonymies multiples probablement...
if not equivalence_file=='':
	logging.info('Retrieving equivalent forms file: '+equivalence_file)
	fonctions.progress(result_path0,10)
	
	try:
		dictionnaire,legende = read_liste2(equivalence_file,read_mode='dict')
		if len(dictionnaire)==0:
			dictionnaire,legende = read_liste2(equivalence_file,read_mode='dict',nd=False)
	except:
		try:
			dictionnaire,legende = read_liste2(equivalence_file,read_mode='dict',nd=False)
		except:
			logging.debug('Please check your tsv file again, is it UTF8 encoded? ' + equivalence_file)
			logger = logging.getLogger()
			logger.handlers[0].flush()
			dksql
	

else:
	dictionnaire,legende={},['custom']

legende = map(lambda x: x.replace('"','').replace(' ','_').replace('/','_').replace('-','_').replace('(','_').replace(')','_').replace('[','_').replace(']','_'),legende)
legende = map(lambda field: re.sub(r'\W+', '', field),legende)
legende_slug = []
for i,leg in enumerate(legende):
	legs=leg
	if len(indexed_table_name)+len(dico_champs_inv.get(field,field)+'_'+ 'custom'+'_')  + len(legs)> 50:
		if len(leg)>21:
			legs=leg[:19]
			legs = legs +  '_'  + str(i)
			
	legende_slug.append(legs)
print 'legende',legende
legende = legende_slug
print 'legende_slug',legende_slug
print "dictionnaire,legende",dictionnaire,legende
#print 'dictionnaire',dictionnaire.keys()
dictionnaire_clean={}


#MAINTENANT:
dictionnaire_consolidated={}
for cle, labels in dictionnaire.iteritems():
	print 'cle,labels',cle,labels
	if cle in dictionnaire_field:#on verifie que la cle fait bien partie de la liste des champs possible
		dictionnaire_consolidated[cle]=dictionnaire[cle]


#AVANT
		#je comprends pas ce passage!!
		#je comprends pas ce passage!!
		#je comprends pas ce passage!!

# for cle, labels in dictionnaire.iteritems():
# 	#print cle,labels
# 	for label in labels:
# 		if cle in dictionnaire_field:#on verifie que la cle fait bien partie de la liste des champs possible
# 			dictionnaire_clean.setdefault(cle,[]).append(label)
# 		else:
# 			#pass
# 			print 'cle absente',cle
# 			# if not cle==label:
# 			# 	if not label in dictionnaire_clean:
# 			# 		dictionnaire_clean[cle]=label
# 			# 	else:
# 			# 		if parameters_user.get("recursive",False):
# 			# 			dictionnaire_clean[cle]=dictionnaire_clean[label]
# 			# 		else:
# 			# 			dictionnaire_clean[cle]=label
# 		if null_tag:
# 			if 'null' in dictionnaire:
# 				dictionnaire_clean['null']=[dictionnaire['null'][0]]
# dictionnaire_consolidated=dictionnaire_clean

print 'dictionnaire size',len(dictionnaire_consolidated)
if len(dictionnaire_consolidated.keys())>0:
	logging.info('dictionnaire size'+  str(len(dictionnaire_consolidated)))
	logging.info('using equivalence form list as provided in the file '+ equivalence_file)
	logging.info('coding new variables:' + pprint.pformat(legende))
	logging.info('including equivalent forms (first 10 elements):')
	for x in dictionnaire_consolidated.keys()[:10]:
		logging.info('\t-' + pprint.pformat(x)+'\t->\t' + pprint.pformat(dictionnaire_consolidated[x]))

else:
	#logging.info("Be careful no matching entity was found in the original list size")	
	pass


#print 'dictionnaire_field',dictionnaire_field
#dictionnaire_consolidated = consolidate_equivalence(dictionnaire,dictionnaire_field)
print 'dictionnaire_consolidated',dictionnaire_consolidated
for x,y in dictionnaire_consolidated.iteritems():
	print x, '\t->\t',dictionnaire_consolidated[x]# liste de la liste (***!) des entités correspondants à chaque colonne
	#logging.info(x+'\t'+pprint.pformat(dictionnaire_consolidated[x]))
print 'listeishere',liste	
#export_file=False	
final_entities={}
forme_2_label={}
for i,leg in enumerate(legende):
	final_entities[leg]={}
	final_list=os.path.join(result_path_list,'list_final'+dico_champs_inv.get(field,field)+'_'+leg+'.tsv')
	print 'final_list',final_list
	myfile = open(final_list, 'wb')
	mywriter = csv.writer(myfile, dialect=dialect)
	mywriter.writerow(['entity','entity label','forms'])
	


	for x in liste:
		if x in dictionnaire_consolidated:
			cle = dictionnaire_consolidated[x][i]
		else:
			cle=[x]
		for cl in cle:
			final_entities[leg].setdefault(cl,[]).append(x)

	if null_tag:
		final_entities[leg][dictionnaire_consolidated.get('null','null')]=['null']

	#print 'final_entities',final_entities
	print 'final_entities ',leg,'size',len(final_entities[leg])
	forme_2_label[leg]={}
	for cle, equivalences in final_entities[leg].iteritems():
		for eq in equivalences:
			forme_2_label[leg].setdefault(eq,[]).append(cle)
		try:
			mywriter.writerow([cle,cle] + ['|&|'.join(equivalences)])
		except:
			pass
	myfile.close()


if indexed_table_name=='ISIpubdate':
	pass
else:
	if len(indexed_table_name)>0:
		indexed_table_name=dico_champs_inv.get(field,field)+'_'+ 'custom'+'_' + indexed_table_name.replace(' ','_')
	else:
		indexed_table_name=dico_champs_inv.get(field,field)+'_'+ 'custom'



conn,curs=fonctions.create_bdd(bdd_name)
corpus = fonctions.get_results(field,curs,rank=True)#on ne considère que les ids déjà indexées dans le champs courant.

if parameters_user.get("assiette",False):
	corpus0 = fonctions.get_results('ISIpubdate',curs,rank=True)#on récupère toutes les ids
	for id in corpus0:
		if not id in corpus: 
			corpus[id]=corpus0[id]
	

#print 'corpus',corpus
file=conn.execute("select file from "+field).fetchall()[0][0]
print file
new_corpus_leg={}
print 'null_tag',null_tag
unique_value_per_entry=parameters_user.get("unique_value_per_entry",False)
forbid={}
#print 'corpus',corpus
#print 'forme_2_label',forme_2_label
for leg in legende:
	new_corpus=[]
	forbid[leg]={}
	for id in corpus:
		#print 'id',id
		for cle in corpus[id]:
			if cle in forme_2_label[leg]:
				for h,val in corpus[id][cle].iteritems():
					#print cle,id,h,val
					#if unique_value_per_entry:
					#	h=0
					for v in val:
						if unique_value_per_entry:
							#print 'isdi'
							for cl in forme_2_label[leg][cle]:
								#print str(file)+'-'+str(id)+'-'+str(h)+'-'+str(cl) 
								#if not str(file)+'-'+str(id)+'-'+str(h)+'-'+str(cl) in forbid:
								if not str(file)+'-'+str(id)+'-'+'-'+str(cl) in forbid[leg]:
									new_corpus.append((file,id,h,v,cl))
								#print (file,id,h,v,forme_2_label[cle])
								#print str(file)+'-'+str(id)+'-'+strx(h)+'-'+str(forme_2_label[cle])
									#forbid[str(file)+'-'+str(id)+'-'+str(h)+'-'+str(cl)]=True
									forbid[leg][str(file)+'-'+str(id)+'-'+'-'+str(cl)]=True
							#else:
							 #	print 'redundant'
						else:
							for cl in forme_2_label[leg][cle]:
								new_corpus.append((file,id,h,v,cl))
					
			else:
				if null_tag:
					for h,val in corpus[id][cle].iteritems():
						#print h,val
						for v in val:
							try:
								#print (file,id,h,v,forme_2_label.get('null','null')[0])
								new_corpus.append((file,id,h,v,forme_2_label[leg].get('null','null')[0]))
							except:
								new_corpus.append((file,id,h,v,forme_2_label[leg].get('null','null')))
	new_corpus_leg[leg]=new_corpus	
#print 'new_corpus',new_corpus
#for ind in new_corpus:
#	print ind
indexed_table_name=indexed_table_name.replace(' ','_').replace('.','_')
print 'icisdopds'
print new_corpus_leg.keys()
for leg,new_corpus in new_corpus_leg.iteritems():
	if len(new_corpus)>0:
		if len(parameters_user.get('indexed_table_name',''))>0 and len(legende)==1:
			indexed_table_nameleg=indexed_table_name
		else:
			print (parameters_user.get('indexed_table_name','').lower())
			if parameters_user.get('indexed_table_name','').lower()=='isipubdate' or leg.lower()=='isipubdate':
				indexed_table_nameleg = 'ISIpubdate'
			else:
				indexed_table_nameleg=indexed_table_name +'_' + leg
		logging.info('Creating the new customized table '+indexed_table_nameleg)
		print ('Creating the new customized table '+indexed_table_nameleg)
		fonctions.progress(result_path0,50)
		conn,cur=create_custom_class(db_name,indexed_table_nameleg)
		fonctions.remplir_table_fast(db_name,indexed_table_nameleg,new_corpus,"( file , id , rank , parserank , data )")
#############################################################################
#############################################################################
##########################-EFFECTIVE CODE-###################################
#############################################################################
#############################################################################





#############################################################################
#############################################################################
##########################-CLI EXPORT-#######################################
#############################################################################
#############################################################################

from librarypy import descriptor
descriptor.generate(bdd_name)
logging.info('List indexation was successfull')
fonctions.progress(result_path0,100)
